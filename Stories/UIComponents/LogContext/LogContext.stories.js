import React, { useState } from 'react';
import { LogProvider, withLogContext } from '../../../src/Components/Contexts/LogContext';
export default { title: 'LogContext' };
import CheckboxField from '../../../src/Components/UIComponents/CheckField/CheckBoxField';

const LogsTestes = ({
    logContext
}) => {
    const {
        Log,
        LogError,
        LogWarn,
        LogInfo,
        LogGroup,
        LogGroupEnd,
        LogCollapsed,
        LogClear
    } = logContext;
    LogClear();
    Log('Log');
    LogError('LogError');
    LogWarn('LogWarn');
    LogInfo('LogInfo');
    LogGroup('Log Group');
    Log('Log');
    LogError('LogError');
    LogWarn('LogWarn');
    LogInfo('LogInfo');
    LogGroupEnd();
    LogCollapsed('Log Group Collapsed');
    Log('Log');
    LogError('LogError');
    LogWarn('LogWarn');
    LogInfo('LogInfo');
    LogGroupEnd();
    return (<div>Logs in console</div>);
}

const Logs = withLogContext(LogsTestes);

export const LogContextNoLogs = () => {
    const [showLog, setShowLog] = useState(false);
    const _onChange = (ev) => {
        setShowLog(ev.target.checked);
    }
    return (
        <div style={{
            padding: '10px' 
        }}>
            <CheckboxField label="Use Logs" checked={showLog} onChange={_onChange} />
            <LogProvider restrictionsLog={showLog ? LOG_RESTRICTIONS.SHOW_ALL : LOG_RESTRICTIONS.SHOW_NONE}>
                <Logs />
            </LogProvider>
        </div>
    );
};

