import React, { useState } from 'react';
import CheckboxField from '../../../src/Components/UIComponents/CheckField/CheckBoxField';
import CheckboxMaterial from '../../../src/Components/UIComponents/MaterialCoreComponents/Checkbox';
import { VARIANTS, ORIENTATION } from '../../../src/Constants/StylesContants';
import { ConatainerLogContextNoLogs, ActionsContainer } from '../../utils';
export default { title: 'CheckBoxField' };

const wrapComponent = (Comp) => (props) => (
    <div style={{
        padding: '10px'
        // , background:'#000' 
    }}>
        <Comp {...props} />
    </div>
);

const Checkbox1 = wrapComponent(CheckboxField);;

export const CheckBoxFieldComponent = () => {
    const [checked, setChecked] = useState(false);
    const _onChange = (ev) => setChecked(ev.target.checked);
    return (
        <ConatainerLogContextNoLogs>
            <ActionsContainer>
                <Checkbox1 label="Check" type='checkbox' checked={checked} onChange={_onChange} />
            </ActionsContainer>
            <Checkbox1 label="Username Label" type='checkbox' checked={checked} disabled />
            <Checkbox1 label="Username Label" type='checkbox' checked={checked} disabled variant={VARIANTS.FILLED} />
            <Checkbox1 label="Username Label" type='checkbox' checked={checked} />
            <Checkbox1 label="Username Label" type='checkbox' checked={checked} variant={VARIANTS.FILLED} />
            <Checkbox1 label="Username Label" type='checkbox' orientation={ORIENTATION.RIGHT_TO_LEFT} checked={checked} />
            <Checkbox1 label="Username Label" type='checkbox' orientation={ORIENTATION.RIGHT_TO_LEFT} checked={checked} variant={VARIANTS.FILLED} />
        </ConatainerLogContextNoLogs>
    );
};


const CheckboxMaterialComponent1 = wrapComponent(CheckboxMaterial);

export const CheckBoxFieldMaterialComponent = () => (
    <div>
        <CheckboxMaterialComponent1 label="Username Label" />
        <CheckboxMaterialComponent1 label="Username Label" />
        <CheckboxMaterialComponent1 label="Username Label" />
    </div>
);
