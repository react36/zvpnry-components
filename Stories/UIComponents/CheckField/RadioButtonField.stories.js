import React, { useState } from 'react';
import RadioButtonField from '../../../src/Components/UIComponents/CheckField/RadioButtonField';
import { VARIANTS } from '../../../src/Constants/StylesContants';
import RadioGroupProvider from '../../../src/Components/UIComponents/CheckField/RadioButtonGroup/RadioGroupProvider';
import { ConatainerLogContextNoLogs, ActionsContainer } from '../../utils';
export default { title: 'RadioButtonField' };

const wrapComponent = (Comp) => (props) => (
    <div style={{
        padding: '10px'
        // , background:'#000' 
    }}>
        <Comp {...props} />
    </div>
);

const RadioButtonField1 = wrapComponent(RadioButtonField);;

export const RadioButtonFieldComponent = () => {
    const [radioButtonsState, setRadioButtonsState] = useState([{ name: 'first', checked: true }]);
    const _onChange = (newState) => {
        setRadioButtonsState(newState)
    }
    return (
        <ConatainerLogContextNoLogs>
            <ActionsContainer>
            </ActionsContainer>
            <RadioGroupProvider onChange={_onChange}>
                <RadioButtonField1 label="Username Label" type='radio' name="teste1" id="first" />
                <RadioButtonField1 label="Username Label" type='radio' name="teste1" id="second" />
            </RadioGroupProvider>
            <RadioButtonField1 label="Username Label" type='radio' name="teste" />
            <RadioButtonField1 label="Username Label" type='radio' name="teste" variant={VARIANTS.FILLED} />
            <RadioButtonField1 label="Username Label" type='radio' variant={VARIANTS.FILLED} />
        </ConatainerLogContextNoLogs>
    );
};

