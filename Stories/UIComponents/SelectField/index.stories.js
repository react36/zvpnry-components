import React, { useState } from 'react';
import { wrapComponent, ActionsContainer } from '../../utils';
import { ConatainerLogContextNoLogs } from '../../utils';
import SelectFieldComponent from '../../../src/Components/UIComponents/SelectField';
export default { title: 'SelectField' };

const StorySelectField = wrapComponent(SelectFieldComponent);

export const SelectField = () => {
    return (
        <ConatainerLogContextNoLogs>
            <ActionsContainer>

            </ActionsContainer>
            <StorySelectField label="Teste"/>
            <StorySelectField label="Teste" disabled />
        </ConatainerLogContextNoLogs>
    );
};
