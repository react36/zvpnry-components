import React, { useState } from 'react';
import TextField from '../../../src/Components/UIComponents/TextField/TextField';
import TextFieldMaterial from '../../../src/Components/UIComponents/MaterialCoreComponents/TextField';
import { VARIANTS } from '../../../src/Constants/StylesContants';
import { wrapComponent, ActionsContainer } from '../../utils';
import { ConatainerLogContextNoLogs } from '../../utils';
export default { title: 'TextField' };

const TextField1 = wrapComponent(TextField);
const TextFieldMaterialComponent1 = wrapComponent(TextFieldMaterial);

export const TextFieldComponent = () => {
    const [value, setValue] = useState('T');
    const _onChange = (ev) => setValue(ev.target.value);
    return (
        <ConatainerLogContextNoLogs>
            <ActionsContainer>
            </ActionsContainer>
            <TextField1 label="Username Label" value={value} onChange={_onChange} />
            <TextField1 label="Username Label" value={value} disabled onChange={_onChange} />
            <TextField1 label="Username Label" value={value} disabled onChange={_onChange} variant={VARIANTS.FILLED} />
            <TextField1 label="Username Label" value={value} disabled onChange={_onChange} variant={VARIANTS.OUTLINED} />
        </ConatainerLogContextNoLogs>
    );
};

export const TextFieldMaterialComponent = () => (
    <div>
        <TextFieldMaterialComponent1 label="Username Label" variant={VARIANTS.STANDARD} />
        <TextFieldMaterialComponent1 label="Username Label" variant={VARIANTS.FILLED} />
        <TextFieldMaterialComponent1 label="Username Label" variant={VARIANTS.OUTLINED} />
    </div>
);
