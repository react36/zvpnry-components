import React, { useState } from 'react';
import { wrapComponent, ActionsContainer } from '../../utils';
import { ConatainerLogContextNoLogs } from '../../utils';
import TemplateComp from '../../../src/Components/UIComponents/_TemplateFolder';
export default { title: 'Template' };

const StoryComponent = wrapComponent(TemplateComp);

export const IndexTemplate = () => {
    return (
        <ConatainerLogContextNoLogs>
            <ActionsContainer>

            </ActionsContainer>
            <StoryComponent />
        </ConatainerLogContextNoLogs>
    );
};
