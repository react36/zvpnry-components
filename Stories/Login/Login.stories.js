import React, { Fragment, useState } from 'react';
import Login from '../../src/Components/Login';
import TextField from '../../src/Components/UIComponents/MaterialCoreComponents/TextField';
import Checkbox from '../../src/Components/UIComponents/MaterialCoreComponents/Checkbox';
import SelectField from '../../src/Components/UIComponents/MaterialCoreComponents/SelectField';
import SelectField1 from '../../src/Components/UIComponents/MaterialCoreComponents/SelectField/SelectField';
import { ConatainerLogContextNoLogs, ActionsContainer } from '../utils';
export default { title: 'Login' };

export const LoginMaterial = () => {

	const [usernameLabel, setUsernameLabel] = useState('Email or UserName');
	const [passwordLabel, setPasswordLabel] = useState('Password');
	const [messageError, setMessageError] = useState('Message error');
	const [showMessageError, setShowMessageError] = useState(false);
	const [smallVersion, setSmallVersion] = useState(false);

	const onChangeUsernameLabel = (ev) => setUsernameLabel(ev.target.value ? ev.target.value : undefined);

	const onChangePasswordLabel = (ev) => setPasswordLabel(ev.target.value ? ev.target.value : undefined);

	const onChangeMessageError = (ev) => setMessageError(ev.target.value ? ev.target.value : undefined);
	const onChangeShowMessageError = (ev) => setShowMessageError(ev.target.checked);
	const onChangeSmallVersion = (ev) => setSmallVersion(ev.target.checked);

	const style = {
		margin: '10px'
	}

	const action = () => (
		<div>
			<TextField
				style={style}
				label="Username Label"
				value={usernameLabel}
				onChange={onChangeUsernameLabel}
			/>
			<TextField
				style={style}
				label="Password Label"
				value={passwordLabel}
				onChange={onChangePasswordLabel}
			/>
			<TextField
				style={style}
				label="Message Error"
				value={messageError}
				onChange={onChangeMessageError}
			/>
			<Checkbox
				style={style}
				label="Show Message Error"
				checked={showMessageError}
				onChange={onChangeShowMessageError}
			/>
			<Checkbox
				style={style}
				label="Show small version"
				checked={smallVersion}
				onChange={onChangeSmallVersion}

			/>
		</div>
	)

	return (
		<ConatainerLogContextNoLogs>
            <ActionsContainer>
			{action()}
            </ActionsContainer>
				
			<Login
				smallVersion={smallVersion}
				usernameLabel={usernameLabel}
				passwordLabel={passwordLabel}
				hasError={showMessageError}
				messageError={messageError}
			/>
		</ConatainerLogContextNoLogs>
	);
}
