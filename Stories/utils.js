import React, { useState } from 'react';
import CheckboxField from '../src/Components/UIComponents/CheckField/CheckBoxField';
import { LogProvider } from '../src/Components/Contexts/LogContext'
import Button from '../src/Components/UIComponents/MaterialCoreComponents/Button';
import { LOG_RESTRICTIONS } from '../src/Constants/MessagesConstants';
export const wrapComponent = (Comp, padding = '5px') => (props) => (
    <div style={{
        padding: '5px'
        // , background:'#000' 
    }}>
        <Comp {...props} />
    </div>
);

export const ConatainerLogContextNoLogs = ({ children }) => {
    const [showLog, setShowLog] = useState(false);
    const _onChange = (ev) => {
        setShowLog(ev.target.checked);
    }
    return (
        <div style={{
            padding: '10px'
            // , background:'#000' 
        }}>
            <ActionsContainer label="ConatainerLogContextNoLogs Component">
                <CheckboxField label="Logs" checked={showLog} onChange={_onChange} />
                <Button style={{ marginLeft: '10px' }} onClick={() => console.clear()}>Clear Console</Button>
            </ActionsContainer>
            <LogProvider restrictionsLog={showLog ? LOG_RESTRICTIONS.SHOW_ALL : LOG_RESTRICTIONS.SHOW_NONE}>
                <div>
                    {children}
                </div>
            </LogProvider>
        </div>
    );
};

export const ActionsContainer = ({ children, label = 'Actions on Story', style, ...rest }) => (
    <fieldset style={{ marginBottom: '10px', padding: '5px', border: '1px solid #ccc', ...style }} {...rest}>
        <legend>{label}</legend>
        {children}
    </fieldset>
);
