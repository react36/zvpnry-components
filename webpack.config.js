const path = require('path');
module.exports = {
	mode: 'production',
	entry: path.resolve(__dirname, 'src', 'index.js'),
	output: {
		filename: 'index.js',
		path: path.resolve(__dirname, 'build'),
		libraryTarget: "commonjs2"
	},
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				loader: 'babel-loader'
			},
			{
				test: /\.css$/i,
				use: ['style-loader', 'css-loader'],
			}
		]
	}/*,
	externals: {
		react: "react",
		'@material-ui/core': "@material-ui/core"
	}*//*,devServer: {
		port: 3000,
		contentBase: path.resolve(__dirname, 'src')
	}*/
}