import React, { useState } from 'react';
import './login.css'
import TextField from '../UIComponents/MaterialCoreComponents/TextField';
import Checkbox from '../UIComponents/MaterialCoreComponents/Checkbox'
import Button from '../UIComponents/MaterialCoreComponents/Button';
import { SOCIAL_ICONS_TYPES } from '../Icons/Constants';
import SocialsMediaIcons from '../Icons/SocialMedia';
import { withLogContext } from '../Contexts/LogContext';

const Login = (
	{
		smallVersion = false,
		onSignIn,
		messageError = 'The username or password you entered are incorrect.',
		hasError = false,
		passwordLabel = 'Password',
		usernameLabel = 'Email Address',
		socialsMedia = [
			SOCIAL_ICONS_TYPES.FACEBOOK,
			SOCIAL_ICONS_TYPES.GOOGLE,
			SOCIAL_ICONS_TYPES.TWITTER
		],
		logContext
	}
) => {
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const [rememberMe, setRememberMe] = useState(false);
	const { Log, LogCollapsed, LogGroupEnd } = logContext;

	const onChangeUsername = (ev) => {
		setUsername(ev.target.value);
	}
	const onChangePassword = (ev) => {
		setPassword(ev.target.value);
	}

	const onChangeRememberMe = (ev) => {
		setRememberMe(ev.target.checked);
	}

	const onClickSignIn = () => {
		LogCollapsed('Login -> onClickSignIn');
		if (onSignIn) {
			onSignIn({
				username,
				password,
				rememberMe
			})
		}
		Log(`Username: ${username}`);
		Log(`Password: ${password}`);
		Log(`Remember Me: ${rememberMe}`);
		LogGroupEnd();
	}

	return (
		<div className='login-content center'>
			{!smallVersion && <p className="header">Sign in</p>}
			<div className="form">
				<TextField
					required
					fullWidth
					id="login_email"
					label={usernameLabel}
					autoComplete="email"
					autoFocus
					value={username}
					onChange={onChangeUsername}
				/>
				<TextField
					required
					fullWidth
					label={passwordLabel}
					type="password"
					id="login_password"
					autoComplete="current-password"
					value={password}
					onChange={onChangePassword}
				/>

				{hasError && <p className="message message-error">{messageError}</p>}
				{!smallVersion && <Checkbox
					value="remember"
					color="primary"
					onChange={onChangeRememberMe}
					label="Remember me"
					checked={rememberMe}
				/>}
				<Button
					fullWidth
					variant="contained"
					color="primary"
					onClick={onClickSignIn}
				>Sign In</Button>
			</div>
			<SocialsMediaIcons
				socialsMedia={socialsMedia}
			/>
			
		</div>
	)
};

export default withLogContext(Login); 