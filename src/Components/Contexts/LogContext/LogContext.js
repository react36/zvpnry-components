import { createContext } from 'react';
import { LOGS_TYPES, LOG_ENVIRONMENT_TYPE, LOG_RESTRICTIONS } from '../../../Constants/MessagesConstants';

export const initialContext = (restrictionsLog = LOG_RESTRICTIONS.SHOW_ALL) => {
	const Log = (message, type = LOG_ENVIRONMENT_TYPE.DEV) => {
		restrictionsLog(type) && LOGS_TYPES.LOG(message);
	}
	const LogError = (message, type = LOG_ENVIRONMENT_TYPE.DEV) => {
		restrictionsLog(type) && LOGS_TYPES.ERROR(message);
	}
	const LogWarn = (message, type = LOG_ENVIRONMENT_TYPE.DEV) => {
		restrictionsLog(type) && LOGS_TYPES.WARNING(message);
	}
	const LogInfo = (message, type = LOG_ENVIRONMENT_TYPE.DEV) => {
		restrictionsLog(type) && LOGS_TYPES.INFO(message);
	}
	const LogGroup = (label, type = LOG_ENVIRONMENT_TYPE.DEV) => {
		restrictionsLog(type) && LOGS_TYPES.GROUP(label);
	}
	const LogGroupEnd = (type = LOG_ENVIRONMENT_TYPE.DEV) => {
		restrictionsLog(type) && LOGS_TYPES.GROUP_END();
	}
	const LogCollapsed = (label, type =LOG_ENVIRONMENT_TYPE.DEV) => {
		restrictionsLog(type) && LOGS_TYPES.GROUP_COLLAPSED(label);
	}
	const LogClear = (type = LOG_ENVIRONMENT_TYPE.DEV) => {
		restrictionsLog(type) && LOGS_TYPES.CLEAR();
	}
	const LogFast = (label, messages = [], type = LOG_ENVIRONMENT_TYPE.DEV) => {
		if (restrictionsLog(type)) {
			LogGroup(label);
			messages.forEach(message => {
				Log(message);
			});
			LogGroupEnd();
		}
	}
	const LogFastType = (label, funcMessageType, messages = [], type = LOG_ENVIRONMENT_TYPE.DEV) => {
		if (restrictionsLog(type)) {
			LogGroup(label);
			messages.forEach(message => {
				funcMessageType(message);
			});
			LogGroupEnd();
		}
	}

	return {
		Log,
		LogError,
		LogWarn,
		LogInfo,
		LogGroup,
		LogGroupEnd,
		LogCollapsed,
		LogClear,
		LogFast,
		LogFastType
	};
};

const LogContext = createContext(initialContext(false));

export const LogContextProvider = LogContext.Provider;
export const LogContextConsumer = LogContext.Consumer;