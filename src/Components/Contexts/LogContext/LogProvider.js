import React from 'react';
import { LogContextProvider, initialContext } from './LogContext';
import { LOG_RESTRICTIONS } from '../../../Constants/MessagesConstants';

const LogProvider = ({ children, restrictionsLog = LOG_RESTRICTIONS.SHOW_NONE }) => {
	return (
		<LogContextProvider value={initialContext(restrictionsLog)} >
			{children}
		</LogContextProvider >
	);
};

export default LogProvider;