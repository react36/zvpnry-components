import React from 'react';
import { LogContextConsumer } from './LogContext';

const withLogContext = (WrappedComponent) => (props) => (
	<LogContextConsumer>
		{
			logContext => <WrappedComponent {...props} logContext={logContext} />
		}
	</LogContextConsumer>
)

export default withLogContext;