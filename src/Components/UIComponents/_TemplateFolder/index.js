import React, { useEffect } from 'react';
import classesContainerWrapper from '../ClassesContainer/ClassesContainer'
import './index.css';
import './style.css';
import './size.css';
import { VARIANTS, COLORS_TYPES } from '../../../Constants/StylesContants';
import { withLogContext } from '../../Contexts/LogContext';

const ComponnentName = (
    {
        propClassesContainer,
        variant = VARIANTS.STANDARD,
        typeColor= COLORS_TYPES.PRIMARY,
        logContext,
        ...rest
    }
) => {
    const {
        Log,
        LogError,
        LogWarn,
        LogInfo,
        LogGroup,
        LogGroupEnd,
        LogCollapsed,
        LogClear,
        LogFastType,
        LogFast
    } = logContext;
    const { addClass, addClasses, removeClass, getClassesString } = propClassesContainer;

    useEffect(() => {
        addClasses(['component-name', `component-name-${variant}`, typeColor]);
    }, []);

    return (
        <div
            className={getClassesString({})}
        >
            Template
        </div>
    );
};

export default withLogContext(classesContainerWrapper(ComponnentName));