import React from 'react';
import PropTypes from 'prop-types';
import PopoverMaterialUI from '@material-ui/core/Popover';

const Popover = ({
	children,
	anchorOrigin = {
		vertical: 'bottom',
		horizontal: 'left',
	},
	transformOrigin = {
		vertical: 'top',
		horizontal: 'center',
	},
	...rest
}) => (
	<PopoverMaterialUI
		{...rest}
		anchorOrigin={anchorOrigin}
		transformOrigin={transformOrigin}
	>
		{children}
	</PopoverMaterialUI>

);

Popover.propTypes = {
	children: PropTypes.element.isRequired,
	anchorOrigin: PropTypes.object,
	transformOrigin: PropTypes.object
};

export default Popover;
