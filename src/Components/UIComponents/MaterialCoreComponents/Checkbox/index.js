import React from 'react';
import FormControlLabel from '../FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
	root: {
		margin: '0 10px 0 0',
		padding:0
	}
});


const MaterialCheckbox = ({
	id,
	color = 'primary',
	checked = false,
	onCheck,
	disabled = false,
	label = '',
	labelPlacement = 'end',
	labelStyle,
	...rest
}) => (
		<FormControlLabel
			control={(
				<Checkbox
					id={id}
					checked={checked}
					onChange={onCheck}
					color={color}
					disabled={disabled}
					{...rest}
				/>
			)}
			label={label}
			labelPlacement={labelPlacement}
			style={labelStyle}
		/>
	);

MaterialCheckbox.propTypes = {
	id: PropTypes.string,
	name: PropTypes.string,
	color: PropTypes.string,
	checked: PropTypes.bool,
	onCheck: PropTypes.func,
	disabled: PropTypes.bool,
	label: PropTypes.object,
	labelPlacement: PropTypes.string,
	labelStyle: PropTypes.object,
};

export default withStyles(styles)(MaterialCheckbox);
