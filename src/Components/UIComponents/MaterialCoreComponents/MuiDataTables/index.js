import React from 'react';
import PropTypes from 'prop-types';
import MUIDataTable from 'mui-datatables';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';


class DataTable extends React.Component {
	getMuiTheme = (outerTheme) => {
		const {
			tableBodyCellMaxWidth,
			actionColumnWidth
		} = this.props;

		return createMuiTheme({
			...outerTheme,
			overrides: {
				MUIDataTable: {},
				MUIDataTableHead: {},
				MUIDataTableHeadCell: {},
				MUIDataTableHeadRow: {},
				MUIDataTableBody: {},
				MUIDataTableBodyRow: {},
				MUIDataTableBodyCell: {
					root: {
						maxWidth: tableBodyCellMaxWidth,
						'&:last-child': {
							width: actionColumnWidth
						}
					}
				},
				MUIDataTablePagination: {},
				MUIDataTableViewCol: {},
				MUIDataTableSearch: {},
				MUIDataTableToolbar: {},
				MUIDataTableFilter: {},
				MUIDataTableFilterList: {}
			}
		});
	}

	render() {
		const {
			id,
			title = '',
			data = [],
			columns = [],
			options,
			pagination = true,
			showDownload = false, // show download icon button
			showPrint = false, // show print icon button
			selectableRows = 'none', // multiple, single, none
			rowsPerPage = 15, // number of rows displayed by page
			filterType = 'multiselect', // filter type can be multiselect, texfield, etc.
			...rest
		} = this.props;

		return (
			<MuiThemeProvider theme={outerTheme => ({ ...this.getMuiTheme(outerTheme) })}>
				<div id={id}>
					<MUIDataTable
						title={title}
						data={data}
						columns={columns}
						options={{
							filterType,
							download: showDownload,
							print: showPrint,
							selectableRows,
							responsive: 'scroll',
							rowsPerPage,
							...options,
						}}
						{...rest}
					/>
				</div>
			</MuiThemeProvider>
		);
	}
}

DataTable.propTypes = {
	id: PropTypes.string.isRequired,
	title: PropTypes.string,
	data: PropTypes.array.isRequired,
	columns: PropTypes.array.isRequired,
	options: PropTypes.object,
	pagination: PropTypes.bool,
	showDownload: PropTypes.bool,
	showPrint: PropTypes.bool,
	selectableRows: PropTypes.string,
	rowsPerPage: PropTypes.number,
	filterType: PropTypes.string,
	tableBodyCellMaxWidth: PropTypes.string || PropTypes.number,
	actionColumnWidth: PropTypes.string || PropTypes.number
};

export default DataTable;
