import React from 'react';
import PropTypes from 'prop-types';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MaterialSwitch from '@material-ui/core/Switch';

const Switch = ({
	id,
	color = 'primary',
	checked = false,
	onChange,
	disabled = false,
	label = '',
	...rest
}) => (
	<FormControlLabel
		control={(
			<MaterialSwitch
				id={id}
				color={color}
				checked={checked}
				onChange={onChange}
				disabled={disabled}
				{...rest}
			/>
		)}
		id={id}
		label={label}
	/>
);

Switch.propTypes =  {
	id: PropTypes.string,
	name: PropTypes.string,
	color: PropTypes.string,
	checked: PropTypes.bool,
	onChange: PropTypes.func,
	disabled: PropTypes.bool,
	label: PropTypes.object,
};

export default Switch;
