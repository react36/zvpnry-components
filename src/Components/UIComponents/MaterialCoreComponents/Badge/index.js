import React from 'react';
import PropTypes from 'prop-types';
import MaterialBadge from '@material-ui/core/Badge';

const Badge = ({
	classes,
	children,
	badgeContent,
	color = 'primary',
	...rest
}) => (
	<MaterialBadge
		{...rest}
		className={classes.margin}
		badgeContent={badgeContent}
		color={color}
	>
		{children}
	</MaterialBadge>
);

Badge.propTypes = {
	classes: PropTypes.object,
	children: PropTypes.element.isRequired,
	badgeContent: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number
	]),
	color: PropTypes.string
};

export default Badge;
