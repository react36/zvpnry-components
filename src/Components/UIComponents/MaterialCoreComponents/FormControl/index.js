import React from 'react';
import MaterialFormControlLabel from "@material-ui/core/FormControlLabel";
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
	root: {
		margin: 0,
		padding: 0
	}
});

const FormControlLabel = (props) => (

	<MaterialFormControlLabel {...props} />
);


export default withStyles(styles)(FormControlLabel);