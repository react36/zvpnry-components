import React from 'react';
import PropTypes from 'prop-types';

import MaterialCollapse from '@material-ui/core/Collapse';

const Collapse = ({
	isOpen,
	timeout = 'auto',
	children
}) => (
	<MaterialCollapse
		in={isOpen}
		timeout={timeout}
		unmountOnExit
	>
		{ children }
	</MaterialCollapse>
);

Collapse.propTypes = {
	isOpen: PropTypes.bool.isRequired,
	timeout: PropTypes.string,
	children: PropTypes.element.isRequired
};

export default Collapse;
