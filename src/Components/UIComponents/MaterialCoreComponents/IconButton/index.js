import React from 'react';
import PropTypes from 'prop-types';
import MaterialIconButton from '@material-ui/core/IconButton';

const IconButton = ({
	id,
	label,
	children,
	color = 'primary',
	disabled = false,
	onClick,
	...rest
}) => (
	<MaterialIconButton
		id={id}
		aria-label={label}
		color={color}
		disabled={disabled}
		onClick={onClick}
		{...rest}
	>
		{children}
	</MaterialIconButton>
);

IconButton.propTypes = {
	id: PropTypes.string,
	label: PropTypes.string,
	children: PropTypes.object,
	color: PropTypes.string,
	disabled: PropTypes.bool,
	onClick: PropTypes.func,
};

export default IconButton;
