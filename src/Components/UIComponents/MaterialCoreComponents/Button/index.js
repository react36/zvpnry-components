import React from 'react';
import PropTypes from 'prop-types';
import MaterialButton from '@material-ui/core/Button';

const Button = ({
	variant = 'contained',
	color = 'primary',
	disabled = false,
	children,
	...rest
}) => (
	<MaterialButton
		variant={variant}
		disabled={disabled}
		color={color}
		{...rest}
	>
		{children}
	</MaterialButton>
);

Button.propTypes = {
	id: PropTypes.string,
	variant: PropTypes.string,
	className: PropTypes.string,
	href: PropTypes.string,
	component: PropTypes.string,
	disabled: PropTypes.bool,
	onClick: PropTypes.func,
	children: PropTypes.any,
	color: PropTypes.string,
};;

export default Button;
