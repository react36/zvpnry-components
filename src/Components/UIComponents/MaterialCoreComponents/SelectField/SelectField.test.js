/* global describe beforeEach afterEach beforeAll afterAll it expect:true */
import React from 'react';
import { shallow } from 'enzyme';

import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';

import SelectField from './SelectField.js';

describe('SelectField Test', () => {
	const component = (
		<SelectField
			id="TestId"
			value="teste"
			onChange={() => (true)}
			hintText="TestSelectField"
			menuItems={[
				{
					payload: 'teste',
					text: 'teste'
				}
			]}
		/>
	);

	it('render selectField ', () => {
		const wrapComponent = shallow(component).dive();
		expect(wrapComponent.find(FormControl).length).toBe(1);
		expect(wrapComponent.find(Select).length).toBe(1);
		expect(wrapComponent.find(FormHelperText).length).toBe(0);
		expect(wrapComponent.find(MenuItem).length).toBe(1);
		expect(wrapComponent.find('#TestId').length).toBe(1);
	});

	it('render selectField with errors', () => {
		const _component = (
			<SelectField
				id="TestId"
				value={null}
				error="Please select a value"
				onChange={() => (true)}
				hintText="TestSelectField"
				menuItems={[
					{
						payload: 'teste',
						text: 'teste'
					}
				]}
			/>
		);
		const wrapComponent = shallow(_component).dive();
		expect(wrapComponent.find(FormControl).length).toBe(1);
		expect(wrapComponent.find(Select).length).toBe(1);
		expect(wrapComponent.find(FormHelperText).length).toBe(1);
		expect(wrapComponent.find(MenuItem).length).toBe(1);
		expect(wrapComponent.find('#TestId').length).toBe(1);
	});
});
