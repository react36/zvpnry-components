import React from 'react';
import PropTypes from 'prop-types';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';
import Checkbox from '../Checkbox';

const styles = theme => ({
	formControl: {
		width: '100%',
	},
	chips: {
		display: 'flex',
		flexWrap: 'wrap',
	},
	chip: {
		margin: theme.spacing(0.25),
	},
});

class SelectField extends React.Component {
	constructor() {
		super();

		this.state = {
			labelWidth: 0
		};
	}

	componentDidMount() {
		this.setState({
			labelWidth: this.InputLabelRef ? this.InputLabelRef.offsetWidth : 0,
		});
	}

	render() {
		const { labelWidth } = this.state;
		const { id, value, error = false, onChange, hintText, menuItems, classes, isMultiSelect = false, customRenderValue, formControlStyle, ...rest } = this.props;
		return (
			<FormControl error={error || false} variant="outlined" className={classes.formControl} style={formControlStyle}>
				<InputLabel
					ref={ref => {
						this.InputLabelRef = ref;
					}}
					htmlFor={`${hintText}_selectField_input_id`}
				>
					{hintText}
				</InputLabel>
				<Select
					id={id}
					value={value}
					onChange={onChange}
					hintText={hintText}
					variant="outlined"
					multiple={isMultiSelect}
					inputProps={{
						name: id,
						id: `${hintText}_selectField_input_id`,
					}}
					labelWidth={labelWidth}
					renderValue={isMultiSelect ? customRenderValue : false}
					{...rest}
				>
					{
						!isMultiSelect && menuItems.map((item, i) => (<MenuItem id={item.id} key={i} value={item.value} disabled={item.disabled}>{item.text}</MenuItem>))
					}
					{
						isMultiSelect && menuItems.map((item, i) => (
							<MenuItem id={item.id} key={i} value={item.value} disabled={item.disabled}>
								<Checkbox checked={value.indexOf(item.value) > -1} />
								{item.text}
							</MenuItem>
						))
					}
				</Select>
				{
					error && (
						<FormHelperText>{error}</FormHelperText>
					)
				}
			</FormControl>
		);
	}
}

SelectField.propTypes = {
	id: PropTypes.string.isRequired,
	value: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number
	]),
	error: PropTypes.any,
	onChange: PropTypes.func.isRequired,
	hintText: PropTypes.string.isRequired,
	menuItems: PropTypes.array.isRequired,
	classes: PropTypes.object,
	isMultiSelect: PropTypes.bool,
	customRenderValue: PropTypes.func,
	formControlStyle: PropTypes.object
};

export default withStyles(styles)(SelectField);
