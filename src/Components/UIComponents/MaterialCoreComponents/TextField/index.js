import React from 'react';
import PropTypes from 'prop-types';
import MaterialTextField from '@material-ui/core/TextField';

const TextField = ({
	type = 'text',
	errorText = '',
	hasError = false,
	variant = 'filled',
	rowsMax = null,
	style = {},
	...rest
}) => (
		<MaterialTextField
			error={hasError}
			type={type}
			helperText={hasError ? errorText : ''}
			style={{ minWidth: '100px', ...style }}
			variant={variant}
			rows={rowsMax}
			{...rest}
		/>
	);

TextField.propTypes = {
	type: PropTypes.string,
	hasError: PropTypes.bool,
	errorText: PropTypes.string,
	variant: PropTypes.string,
	rowsMax: PropTypes.number,
	style: PropTypes.object
};
export default TextField;
