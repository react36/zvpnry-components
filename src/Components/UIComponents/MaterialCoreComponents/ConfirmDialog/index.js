import React from 'react';
import PropTypes from 'prop-types';
import MaterialDialog from '@material-ui/core/Dialog';
import MaterialDialogActions from '@material-ui/core/DialogActions';
import MaterialTypography from '@material-ui/core/Typography';

import Button from '../Button';

import DialogTitle from './DialogTitle'
import DialogContent from './DialogContent';

const ConfirmDialog = (props) => {
	const {
		cancelLabel,
		confirmLabel,
		title,
		open,
		hasConfirm = true,
		onClose,
		children,
		onConfirmClicked,
		isDisabled,
		modal = false,
		autoScrollBodyContent,
		dialogProps,
		id = 'confirm_dialog'
	} = props;

	return (
		<MaterialDialog
			open={open}
			fullWidth
			maxWidth="md"
			onClose={onClose}
			disableBackdropClick={modal}
			disableEscapeKeyDown={modal}
			autoScrollBodyContent={autoScrollBodyContent}
			role="dialog"
			{...dialogProps}
		>
			<DialogTitle
				id="dialog-title"
				onClose={onClose}
			>
				{title}
			</DialogTitle>
			<DialogContent>
				<MaterialTypography component="div">
					{children}
				</MaterialTypography>
			</DialogContent>
			<MaterialDialogActions>
				<Button
					onClick={onClose}
					variant="text"
					color="inherit"
					id={`cancel_${id}`}
				>
					{cancelLabel || 'Cancel'}
				</Button>
				{
					hasConfirm && (
						<Button
							onClick={onConfirmClicked}
							variant="text"
							color="primary"
							disabled={isDisabled}
							id={`add_${id}`}
						>
							{confirmLabel || 'Apply'}
						</Button>
					)
				}
			</MaterialDialogActions>
		</MaterialDialog>
	);
};

ConfirmDialog.propTypes = {
	onClose: PropTypes.func,
	onConfirmClicked: PropTypes.func,
	cancelLabel: PropTypes.string,
	confirmLabel: PropTypes.string,
	title: PropTypes.string,
	open: PropTypes.bool,
	modal: PropTypes.bool,
	isDisabled: PropTypes.bool,
	children: PropTypes.object,
	hasConfirm: PropTypes.bool,
	autoScrollBodyContent: PropTypes.bool,
	dialogProps: PropTypes.object,
	id: PropTypes.string
};

export default ConfirmDialog;
