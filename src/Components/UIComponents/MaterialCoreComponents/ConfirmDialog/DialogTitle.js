import React from 'react';
import MaterialMuiDialogTitle from '@material-ui/core/DialogTitle';
import MaterialCloseIcon from '@material-ui/icons/Close';
import MaterialTypography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import IconButton from '../IconButton';

const styles = theme => ({
	root: {
		borderBottom: `1px solid ${theme.palette.divider}`,
		margin: 0,
		padding: theme.spacing(2),
	},
	closeButton: {
		position: 'absolute',
		right: theme.spacing(1),
		top: theme.spacing(1),
		color: theme.palette.grey[500],
	},
});

const DialogTitle = ({ children, classes, onClose }) => (
	<MaterialMuiDialogTitle disableTypography className={classes.root}>
		<MaterialTypography variant="h6">{children}</MaterialTypography>
		{onClose ? (
			<IconButton
				label="Close"
				className={classes.closeButton}
				onClick={onClose}
			>
				<MaterialCloseIcon />
			</IconButton>
		) : null}
	</MaterialMuiDialogTitle>
);
export default withStyles(styles)(DialogTitle);
