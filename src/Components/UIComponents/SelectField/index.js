import React, { useEffect, useState } from 'react';
import classesContainerWrapper from '../ClassesContainer/ClassesContainer'
import './index.css';
import './style.css';
import './size.css';

import TextField from '../TextField/TextField'

import { VARIANTS, COLORS_TYPES } from '../../../Constants/StylesContants';
import { withLogContext } from '../../Contexts/LogContext';

const SelectField = (
    {
        id = '',
        propClassesContainer,
        variant = VARIANTS.STANDARD,
        typeColor = COLORS_TYPES.PRIMARY,
        logContext,
        ...rest
    }
) => {

    const { addClass, addClasses, removeClass, getClassesString } = propClassesContainer;
    const {
        Log,
        LogError,
        LogWarn,
        LogInfo,
        LogGroup,
        LogGroupEnd,
        LogCollapsed,
        LogClear,
        LogFastType,
        LogFast
    } = logContext;

    useEffect(() => {
        addClasses(['select-field', `select-field-${variant}`, typeColor]);
    }, []);

    const [selectedOption, setSelectedOption] = useState(undefined);
    const _onFocus = (ev) => {
        LogFast(`SelectField || ${id} || _onFocus`, ['Just show the dropDown', ev]);
        addClass('dropdow-visible');
    }

    const _onBlur = (ev) => {
        LogFast(`SelectField || ${id} || _onBlur`, ['Just hide the dropDown', ev]);
        removeClass('dropdow-visible');
    }

    const _onSelect = (option) => {
        LogFast(`SelectField || ${id} || _onSelect`, ['Just select option']);
        setSelectedOption(option);
    }
    const _value = selectedOption ? selectedOption.value : undefined;
    return (
        <div className={getClassesString()}>
            <TextField
                id={`${id}-text-field`}
                variant={variant}
                typeColor={typeColor}
                onBlur={_onBlur}
                onFocus={_onFocus}
                value={_value}
                {...rest}
            >
                <i className="chevron-icon fas fa-chevron-down"></i>
            </TextField>
            <ul className="list">
                <li className="item"><fieldset onClick={_onSelect}>teste</fieldset></li>
                <li className="item"><div onClick={_onSelect}>teste1</div></li>
                <li className="item"><div onClick={_onSelect}>teste2</div></li>
                <li className="item"><div onClick={_onSelect}>teste3</div></li>
            </ul>
        </div>
    );
};

export default withLogContext(classesContainerWrapper(SelectField));