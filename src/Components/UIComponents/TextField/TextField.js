import React, { useRef, useEffect, useState, Children } from 'react';
import './index.css';
import './style.css';
import './size.css';
import { COLORS_TYPES, VARIANTS } from '../../../Constants/StylesContants';
import withStyleContext from '../../../Contexts/StyleContext/Context/WithStyleContext';
import classesContainerWrapper from '../ClassesContainer/ClassesContainer';

const TextField = (
	{
		className = '',
		typeColor = COLORS_TYPES.PRIMARY,
		styleContext,
		onBlur,
		onFocus,
		onchange,
		label,
		variant = VARIANTS.STANDARD,
		style = {},
		value,
		propClassesContainer,
		children,
		...rest
	}
) => {
	const textInput = useRef(null);

	const [valueField, setValue] = useState('');
	const { addClass, addClasses, removeClass, getClassesString } = propClassesContainer;

	useEffect(() => {
		addClasses(['text-field', `text-field-${variant}`, typeColor]);
	}, []);

	const onClick = () => {
		textInput.current.focus()
	}

	const _onBlur = (ev) => {
		removeClass('focus');
		if (onBlur)
			onBlur(ev);
	}

	const _onFocus = (ev) => {
		addClass('focus')
		if (onFocus)
			onFocus(ev);
	}

	const _onchange = (ev) => {
		ev.target.value ? addClass('filled') : removeClass('filled');
		setValue(ev.target.value);
		if (onchange) {
			onchange(ev);
		}
	}
	const _value = value === undefined ? valueField : value;
	return (
		<fieldset className={getClassesString({ filled: _value })} onClick={onClick} style={style}>
			<legend>{label}</legend>
				<input
					ref={textInput}
					onChange={_onchange}
					onFocus={_onFocus}
					onBlur={_onBlur}
					placeholder={label}
					value={_value}
					{...rest}
				/>
				{children}
		</fieldset>
	);
}

export default withStyleContext(classesContainerWrapper(TextField));