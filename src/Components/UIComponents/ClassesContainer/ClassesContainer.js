import React, { useState } from 'react';

const classesContainerWrapper = (Comp) => ({ disabled, typeColor, stateType, className, ...props }) => {
    const [classes, setClass] = useState([]);

    const addClass = (paramclassName) => {
        if (!classes.includes(paramclassName)) {
            setClass([...classes, paramclassName])
        }
    };

    const addClasses = (classesNames = []) => {
        const notExistsClasses = classesNames.filter(obj => {
            return !classes.includes(obj)
        });
        setClass([...classes, ...notExistsClasses]);
    }

    const removeClass = (paramclassName) => setClass(classes.filter(obj => obj != paramclassName));


    const getClassesString = (params = {}) => {
        const { filled, checked } = params
        let returnClasses = classes.reduce((accumulator, currentValue) => `${accumulator} ${currentValue}`, '');
        returnClasses += disabled ? ' disabled' : '';
        returnClasses += filled ? ' filled' : '';
        returnClasses += stateType ? ` ${stateType}` : '';
        returnClasses += className ? ` ${className} ` : '';
        returnClasses += checked ? ' checked' : '';
        return returnClasses || '';
    };

    return (
        <Comp
            propClassesContainer={
                {
                    addClass,
                    addClasses,
                    removeClass,
                    getClassesString
                }
            }
            {...props}
            disabled={disabled}
        />
    );
};

export default classesContainerWrapper;