import PropTypes from 'prop-types';

const FeatureToggle = ({ untoggledComponent, children, funcCondition }) => {
	if (funcCondition && funcCondition()) {
		return (children);
	}
	if (untoggledComponent) {
		return (untoggledComponent);
	}
	return null;
};

FeatureToggle.propTypes = {
	funcCondition: PropTypes.func.isRequired,
	children: PropTypes.element.isRequired,
	untoggledComponent: PropTypes.element.isRequired,
};

export default FeatureToggle;
