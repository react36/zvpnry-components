import { createContext } from 'react';

export const initialContex = {
    onChangeRadioGroup: () => console.log('RadioGroup not configured'),
    checkedGroup: []
}
const RadioGroupContext = createContext(initialContex);

export const RadioGroupContextProvider = RadioGroupContext.Provider;
export const RadioGroupContextConsumer = RadioGroupContext.Consumer;