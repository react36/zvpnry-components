import React, { useState } from 'react';
import { RadioGroupContextProvider } from './RadioGroupContext';

const RadioGroupProvider = ({
	children,
	onChange,
	radioButtonsState
}) => {
	const [checkedGroup, setCheckedGroup] = useState([]);

	const changeChecked = (id, checkedGroupParam) => {

		const newCheckedGroup = checkedGroupParam.map(obj => (
			{
				name: obj.name,
				checked: obj.name === id
			}
		));
		if (!newCheckedGroup.find(checkedField => checkedField.name === id)) {
			newCheckedGroup.push({ name: id, checked: true })
		}
		return newCheckedGroup;
	}

	const onChangeRadioGroup = (ev) => {
		const { id } = ev.target;
		let newCheckedGroup = [];
		if (radioButtonsState) {
			newCheckedGroup = changeChecked(id, radioButtonsState);
		} else {
			newCheckedGroup = changeChecked(id, checkedGroup);
			setCheckedGroup(newCheckedGroup);
		}		
		onChange(newCheckedGroup);
	}

	return (
		<RadioGroupContextProvider value={{ onChangeRadioGroup, checkedGroup: radioButtonsState || checkedGroup }}>
			{children}
		</RadioGroupContextProvider >

	);
};

export default RadioGroupProvider;