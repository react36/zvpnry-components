import React from 'react';
import { RadioGroupContextConsumer } from './RadioGroupContext';

const WithRadioGroupContext = (WrappedComponent) => (props) => (
	<RadioGroupContextConsumer>
		{
			radioGroupContex => <WrappedComponent {...props} radioGroupContex={radioGroupContex} />
		}
	</RadioGroupContextConsumer>
)

export default WithRadioGroupContext;