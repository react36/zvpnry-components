import React, { useRef, useState, useEffect } from 'react';
import './index.css';
import './style.css';
import './size.css';
import classesContainerWrapper from '../../ClassesContainer/ClassesContainer';
import { ORIENTATION, VARIANTS, COLORS_TYPES } from '../../../../Constants/StylesContants';

const CheckboxField = ({
    propClassesContainer,
    checked,
    type = "checkbox",
    variant = VARIANTS.STANDARD,
    typeColor = COLORS_TYPES.PRIMARY,
    orientation = ORIENTATION.LEFT_TO_RIGHT,
    checkmarkIcon,
    label,
    ...rest
}) => {
    const checkboxRef = useRef(null);
    const { addClass, addClasses, removeClass, getClassesString } = propClassesContainer;

    useEffect(() => {
        addClasses(['check-field', `check-field-${variant}`, typeColor, orientation]);
    }, []);

    const _onClick = () => {
        checkboxRef.current.click();
    }

    return (
        <div className={getClassesString({ checked })} onClick={_onClick}>
            <span className={`icon-${type}`}>
                <i className={`checkmark ${checkmarkIcon}`}></i>
            </span>
            <label>{label}</label>
            <input ref={checkboxRef}
                type={type}
                data-checked={checked}
                checked={checked}
                {...rest}
            ></input>
        </div>
    )
}

export default classesContainerWrapper(CheckboxField);