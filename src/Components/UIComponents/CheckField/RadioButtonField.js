import React, { useState } from 'react';
import CheckField from './CheckField/CheckField';
import WithRadioGroupContext from './RadioButtonGroup/WithRadioGroupContext';
import { withLogContext } from '../../Contexts/LogContext'
const RadioButtonField = (
    {
        id,
        checkmarkIcon = 'fas fa-circle',
        type,
        checked,
        onChange,
        radioGroupContex,
        logContext,
        ...rest
    }
) => {
    const {
        Log,
        LogError,
        LogWarn,
        LogInfo,
        LogGroup,
        LogGroupEnd,
        LogCollapsed,
        LogClear,
        LogFastType,
        LogFast
    } = logContext;

    const { onChangeRadioGroup, checkedGroup } = radioGroupContex;

    let _checked = checked || false;
    if (checked === undefined && checkedGroup) {
        const findCheckedGroup = checkedGroup.find(chedkedField => chedkedField.name === id);
        _checked = findCheckedGroup ? findCheckedGroup.checked : false;
    }

    const _onChange = (ev) => {

        LogFast(`RadioButton || ${id} || _onChange`, ['teste', ev]);
        if (radioGroupContex) {
            onChangeRadioGroup(ev);
        }
        if (onChange) {
            onChange(ev);
        }

    }

    return (
        <CheckField
            id={id}
            {...rest}
            checkmarkIcon={checkmarkIcon}
            type='radio'
            checked={_checked}
            onChange={_onChange}
        />
    );
}

export default withLogContext(WithRadioGroupContext(RadioButtonField));