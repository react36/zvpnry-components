import React, { useState } from 'react';
import CheckField from './CheckField/CheckField';
import withLogContext from '../../Contexts/LogContext/withLogContext';

const CheckboxField = ({
    checkmarkIcon = 'fas fa-check',
    type,
    checked,
    onChange,
    logContext,
    ...rest }) => {

    const [isChecked, setIsChecked] = useState(false);

    const _checked = checked === undefined ? isChecked : checked;

    const _onChange = (ev) => {
        if (onChange) {
            onChange(ev);
        }
        setIsChecked(ev.target.checked);
    }
    return (<CheckField
        {...rest}
        checkmarkIcon={checkmarkIcon}
        type='checkbox'
        checked={_checked}
        onChange={_onChange}
    />
    )
}



export default withLogContext(CheckboxField);