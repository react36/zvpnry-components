import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import './index.css'
const SocialIcon = ({ type, className = '', ...rest }) =>
	(
		<span
			className={`.icon-awesome ${className}`}
			style={
				{ backgroundColor: type.color }
			}>
			<FontAwesomeIcon
				icon={type.icon}
				{...rest} />
		</span>
	);

export default SocialIcon;

