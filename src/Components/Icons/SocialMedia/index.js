import React from 'react';
import SocialIcon from './SocialIcon';

const SocialsMediaIcons = (
	{
		socialsMedia,
		...rest
	}
) => (
		<div className="social-media" {...rest}>
			{
				socialsMedia.map((socialMedia, index) => {
					if (socialMedia.type) {
						return (<SocialIcon key={`socialIcon-${index}`} {...socialMedia} />)
					}
					return (<SocialIcon key={`socialIcon-${index}`} type={socialMedia} />)
				})
			}
		</div>
	);

export default SocialsMediaIcons;