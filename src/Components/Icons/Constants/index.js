import {
	faGoogle,
	faFacebookF,
	faTwitter
} from '@fortawesome/free-brands-svg-icons';

export const SOCIAL_ICONS_TYPES = {
	FACEBOOK: {
		id: 'facebook',
		icon: faFacebookF,
		color: '#3a559f'
	},
	TWITTER: {
		icon: faTwitter,
		color: '#4ea6ea'
	},
	GOOGLE: {
		icon: faGoogle,
		color: '#e0584a'
	}
}