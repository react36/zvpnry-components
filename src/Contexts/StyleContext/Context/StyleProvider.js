import React from 'react';
import { StyleContexProvider } from './StyleContext';

const StyleProvider = ({ children, ...rest }) => (
	<StyleContexProvider value={...rest}>
		{children}
	</StyleContexProvider>
);

export default StyleProvider;