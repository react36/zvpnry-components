import { createContext } from 'react';
import { COLORS_TYPES } from '../../../Constants/StylesContants';

export const initialContex = {
	colorsType: COLORS_TYPES.PRIMARY
}

const styleContext = createContext(initialContex);

export const StyleContextProvider=styleContext.Provider;
export const StyleContextConsumer=styleContext.Consumer;