import React from 'react';
import { StyleContextConsumer } from './StyleContext';

const withStyleContext = (WrappedComponent) => (props) => (
	<StyleContextConsumer>
		{
			styleContext => <WrappedComponent {...props} styleContext={styleContext} />
		}
	</StyleContextConsumer>
)

export default withStyleContext;