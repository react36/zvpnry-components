export const LOGS_TYPES = {
    LOG: (message) => console.log(message),
    INFO: (message) => console.info(message),
    WARNING: (message) => console.warn(message),
    ERROR: (message) => console.error(message),
    GROUP_END: () => console.groupEnd(),
    GROUP: (label) => console.group(label),
    GROUP_COLLAPSED: (label) => console.groupCollapsed(label),
    CLEAR: () => console.clear()
}

export const LOG_ENVIRONMENT_TYPE = {
    DEV:'dev',
    PROD:'prod'
};

export const LOG_RESTRICTIONS = {
    SHOW_NONE: (type) => false,
    SHOW_ALL: (type) => true,
    SHOW_ONLY_DEV: (type) => type === LOG_ENVIRONMENT_TYPE.DEV,
    SHOW_ONLY_PROD: (type) => type === LOG_ENVIRONMENT_TYPE.PROD
};
