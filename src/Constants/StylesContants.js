export const COLORS_TYPES = {
	PRIMARY: 'primary',
	SECONDARY: 'secondary',
}

export const VARIANTS = {
	STANDARD: 'standard',
	OUTLINED: 'outlined',
	FILLED: 'filled'
}

export const STATE_TYPES = {
	INFO: 'info',
	SUCCESS: 'success',
	WARNING: 'warning',
	ERROR: 'error'
}


export const ORIENTATION = {
	LEFT_TO_RIGHT: 'left-right-orientation',
	RIGHT_TO_LEFT: 'right-left-orientation',
}